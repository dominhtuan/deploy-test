Rails.application.routes.draw do
	
	resources :articles do
		collection do
	    	get 'getListArticlesInJsonFormat'
	  	end
	end

	root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
