require 'json'
class ArticlesController < ApplicationController


  before_action :cors_preflight_check
  after_filter :cors_set_access_control_headers

# For all responses in this controller, return the CORS access control headers.

	def cors_set_access_control_headers
	  headers['Access-Control-Allow-Origin'] = '*'
	  headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
	  headers['Access-Control-Allow-Headers'] = '*'
	  headers['Access-Control-Max-Age'] = "1728000"
	end

	# If this is a preflight OPTIONS request, then short-circuit the
	# request, return only the necessary headers and return an empty
	# text/plain.

	def cors_preflight_check
	  if request.method == :options
	    headers['Access-Control-Allow-Origin'] = '*'
	    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
	    headers['Access-Control-Allow-Headers'] = '*'
	    headers['Access-Control-Max-Age'] = '1728000'
	    render :text => '', :content_type => 'text/plain'
	  end
	end

	def index
    	@articles = Article.all
  	end

  	def getListArticlesInJsonFormat
    	@articles = Article.all
    	render json: @articles
  	end

	def show
    	@article = Article.find(params[:id])
  	end

	def new
		@article = Article.new
	end

	def edit
  		@article = Article.find(params[:id])
	end

	def create
	  	@article = Article.new(article_params) 

  		if @article.save
    		redirect_to @article
  		else
    		render 'new'
  		end

	end

	def update
	   	@article = Article.find(params[:id])
	 
	   	if @article.update(article_params)
	    	redirect_to @article
	  	else
	    	render 'edit'
	  	end
	end

	def destroy
		@article = Article.find(params[:id])
		@article.destroy
		
		redirect_to articles_path
	end
 
	private
  	def article_params
    	params.require(:article).permit(:title, :text)
  	end

end
